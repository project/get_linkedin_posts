Get Linkedin Posts
======

Provides functionality for import posts from Linkedin
company's page in Drupal node.

Installation
-------------
This module needs to be installed via Composer, which will download the
required libraries.

1. Add the Drupal Packagist repository

    ```sh
    composer config repositories.drupal composer https://packages.drupal.org/8
    ```
This allows Composer to find Get Linkedin Posts and the other Drupal modules.

2. Download Get Linkedin Posts

   ```sh
   composer require "drupal/get_linkedin_posts ~1.0"
   ```
This will download the latest release of Get Linkedin Posts.
Use 1.x-dev instead of ~1.0 to get the -dev release instead.

3. Config your credentials for Linkedin application.


See https://www.drupal.org/node/2404989 for more information.
